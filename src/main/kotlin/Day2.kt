fun getDepthTimesHorizontal() {
    val inputs = readFileGetStrings("${RESOURCES_DIR}/AdventDay2")
    var horizontal = 0
    var depth = 0

    for (input in inputs) {
        val num = input.last().digitToInt()
        if (input.contains("forward")) {
            horizontal += num
            continue
        }
        if (input.contains("down")) {
            depth += num
            continue
        }
        depth -= num
    }
    println(depth * horizontal)
}

fun getNewDepthTimesHorizontal() {
    val inputs = readFileGetStrings("${RESOURCES_DIR}/AdventDay2")
    var horizontal = 0
    var aim = 0
    var depth = 0

    for (input in inputs) {
        val num = input.last().digitToInt()
        if (input.contains("forward")) {
            horizontal += num
            depth += aim * num
            continue
        }
        if (input.contains("down")) {
            aim += num
            continue
        }
        aim -= num
    }
    println(horizontal * depth)
}