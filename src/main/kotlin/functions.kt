import java.io.File

fun readFileGetInts(fileName: String): List<Int> {
    val readInts: MutableList<Int> = mutableListOf<Int>()
    File(fileName).forEachLine {
        readInts.add(it.toInt())
    }
    return readInts.toList()
}

fun readFileGetStrings(fileName: String): List<String> {
    val readStrings: MutableList<String> = mutableListOf<String>()
    File(fileName).forEachLine {
        readStrings.add(it)
    }
    return readStrings
}