import kotlin.math.pow

fun getGammaEpsilon() {
    val bitInput = readFileGetStrings("$RESOURCES_DIR/AdventDay3")
    val numColumns = bitInput[0].length
    var column = 0
    var gamma = ""
    var epsilon =  ""
    while (column < numColumns) {
        if (getMostCommonBitInCol(bitInput, column) == '0') {
            gamma += '0'
            epsilon += '1'
        } else {
            gamma += '1'
            epsilon += '0'
        }
        column++
    }

    println(binaryToInt(gamma) * binaryToInt(epsilon))

}

fun getOxygenRating() {
    val input = readFileGetStrings("$RESOURCES_DIR/AdventDay3")
    val numColumns = input[0].length
    val oxygenRating = input.toMutableList()
    val dioxideRating = input.toMutableList()

    var column = 0
    while (column < numColumns) {
        // oxygenRating
        if (oxygenRating.size > 1) {
            val mostCommonOxygenBit = getMostCommonBitInCol(oxygenRating, column)
            val leastCommonOxygenBit = if (mostCommonOxygenBit == '1') {
                '0'
            } else {
                '1'
            }
            oxygenRating.removeIf {it[column] == leastCommonOxygenBit}
        }

        //dioxideRating
        if (dioxideRating.size > 1) {
            val mostCommonDioxideBit = getMostCommonBitInCol(dioxideRating, column)
            dioxideRating.removeIf {it[column] == mostCommonDioxideBit}
        }
        column++
    }
    val dioxideRatingNum = binaryToInt(dioxideRating.first())
    val oxygenRatingNum = binaryToInt(oxygenRating.first())
    println(dioxideRating.size)
    println(oxygenRating.size)
    println(dioxideRatingNum * oxygenRatingNum)
}

fun getMostCommonBitInCol(bitStrings: List<String>, column: Int): Char {
    var numZeroes = 0
    var i = 0
    var numOnes = 0
    val numRows = bitStrings.size
    while (i < numRows) {
        if (bitStrings[i][column] == '0') {
            numZeroes++
        } else {
            numOnes++
        }
        i++
    }
    if (numZeroes > numOnes) {
        return '0'
    }
    return '1'
}

fun binaryToInt(binary: String): Int {
    val two = 2.0
    var i = 0
    var sum = 0
    while (i < binary.length) {
        val temp = binary[binary.length - (1 + i)].digitToInt()
        sum += (temp * two.pow(i)).toInt()
        i++
    }
    return sum
}