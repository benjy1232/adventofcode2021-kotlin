fun findTimesDepthInc() {
    val inputs = readFileGetInts("${RESOURCES_DIR}/AdventDay1")
    var i = 1
    var counter = 0

    while (i < inputs.size) {
        if (inputs[i] > inputs[i-1]) {
            counter++
        }
        i++
    }
    println(counter)
}

fun findTimesPrev3DepthInc() {
    val inputs = readFileGetInts("${RESOURCES_DIR}/AdventDay1")
    var i = 3
    var counter = 0
    var prevSum = inputs[2] + inputs[1] + inputs[0]

    while (i < inputs.size) {
        val currentSum = inputs[i] + inputs[i-1] + inputs[i-2]
        if (currentSum > prevSum) {
            counter++
        }
        prevSum = currentSum
        i++
    }
    println(counter)
}