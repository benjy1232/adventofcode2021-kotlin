fun main() {
    // Day 1
//    findTimesDepthInc()
//    findTimesPrev3DepthInc()

    // Day 2
//    getDepthTimesHorizontal()
//    getNewDepthTimesHorizontal()

    // Day 3
    getGammaEpsilon()
    getOxygenRating()
}